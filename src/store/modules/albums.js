import axios from 'axios'

const url = '/photos'
const state = {
    albums: [],
    photos: [],
    album: []
}
const getters = {
    allAlbums: (state) => state.albums,
    allPhotos: (state) => state.photos,
    album: (state) => state.album
}
const actions = {
    getPhotos: async ({ commit }) => {
        const response = await axios.get(url)
        commit('setAlbums', response.data)
    },
    getAlbum: ({ commit }, album) => commit('setPhotos', album)
}
const mutations = {
    setPhotos: (state, photos) => state.photos = photos,
    setAlbums: (state, photos) => {
        const albums = photos.reduce((r, a) => {
            r[a.albumId] = [...r[a.albumId] || [], {...a, name: `Album ${a.albumId}`}]
            return r
        }, {})
        state.albums = Object.values(albums)
    },
    setAlbum: (state, album) => state.album = album
}

export default {
    state,
    getters,
    actions,
    mutations
}