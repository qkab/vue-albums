import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/home/Home'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path:'/',
            name:'home',
            component: Home
        },
        {
            path: '/photos',
            name: 'photos',
            component: () => import('./components/photos/Photos')
        },
    ]
})